/*  HomeBank -- Free, easy, personal accounting for everyone.
 *  Copyright (C) 1995-2022 Maxime DOYEN
 *
 *  This file is part of HomeBank.
 *
 *  HomeBank is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 2 of the License, or
 *  (at your option) any later version.
 *
 *  HomeBank is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "homebank.h"

#include "ui-filter.h"
#include "ui-account.h"
#include "ui-payee.h"
#include "ui-category.h"
#include "gtk-dateentry.h"


/****************************************************************************/
/* Debug macros										 */
/****************************************************************************/
#define MYDEBUG 0

#if MYDEBUG
#define DB(x) (x);
#else
#define DB(x);
#endif

/* our global datas */
extern struct HomeBank *GLOBALS;


extern gchar *CYA_FLT_TYPE[];
extern gchar *CYA_FLT_STATUS[];
extern gchar *CYA_FLT_RANGE[];
extern gchar *CYA_SELECT[];

extern gchar *nainex_label_names[];

extern HbKivData CYA_TXN_PAYMODE[NUM_PAYMODE_MAX];


/* = = = = = = = = = = = = = = = = = = = = */

//#1828732 add expand/collapse all for categories in edit filter
static void ui_flt_hub_category_expand_all(GtkWidget *widget, gpointer user_data)
{
struct ui_flt_manage_data *data;

	data = g_object_get_data(G_OBJECT(gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW)), "inst_data");
	DB( g_print("\n(ui_flt_hub_category) expand all (data=%p)\n", data) );

	gtk_tree_view_expand_all(GTK_TREE_VIEW(data->LV_cat));

}


static void ui_flt_hub_category_collapse_all(GtkWidget *widget, gpointer user_data)
{
struct ui_flt_manage_data *data;

	data = g_object_get_data(G_OBJECT(gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW)), "inst_data");
	DB( g_print("\n(ui_flt_hub_category) collapse all (data=%p)\n", data) );

	gtk_tree_view_collapse_all(GTK_TREE_VIEW(data->LV_cat));

}


static void ui_flt_hub_category_get(Filter *flt, struct ui_flt_manage_data *data)
{
gint i;

	DB( g_print("(ui_flt_hub_category) get\n") );

	if(data->filter !=NULL)
	{
	GtkTreeModel *model;
	//GtkTreeSelection *selection;
	GtkTreeIter	iter, child;
	gint n_child;
	gboolean valid;
	gboolean toggled;


	// category
		DB( g_print(" category\n") );

		model = gtk_tree_view_get_model(GTK_TREE_VIEW(data->LV_cat));
		//selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(data->LV_cat));
		i=0; valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
		while (valid)
		{
		Category *catitem;

			gtk_tree_model_get (model, &iter,
				LST_DEFCAT_TOGGLE, &toggled,
				LST_DEFCAT_DATAS, &catitem,
				-1);

			//data->filter->cat[i] = gtk_tree_selection_iter_is_selected(selection, &iter);
			//data->filter->cat[i] = toggled;
			catitem->flt_select = toggled;

			n_child = gtk_tree_model_iter_n_children (GTK_TREE_MODEL(model), &iter);
			gtk_tree_model_iter_children (GTK_TREE_MODEL(model), &child, &iter);
			while(n_child > 0)
			{
				i++;

				gtk_tree_model_get (model, &child,
					LST_DEFCAT_TOGGLE, &toggled,
					LST_DEFCAT_DATAS, &catitem,
					-1);

				DB( g_print(" subcat k:%3d = %d (%s)\n", catitem->key, toggled, catitem->name) );
				da_flt_status_cat_set(flt, catitem->key, toggled);

				//data->filter->cat[i] = toggled;
				//data->filter->cat[i] = gtk_tree_selection_iter_is_selected(selection, &child);
				catitem->flt_select = toggled;

				n_child--;
				gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &child);
			}

			/* Make iter point to the next row in the list store */
			i++; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
		}

	}
}


static void ui_flt_hub_category_set(Filter *flt, struct ui_flt_manage_data *data)
{

	DB( g_print("(ui_flt_hub_category) set\n") );

	if(data->filter != NULL)
	{
	GtkTreeModel *model;
	//GtkTreeSelection *selection;
	GtkTreeIter	iter, child;

	gint n_child;
	gboolean valid;
	gint i;


	// category
		DB( g_print(" category\n") );

		model = gtk_tree_view_get_model(GTK_TREE_VIEW(data->LV_cat));
		//selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(data->LV_cat));
		i=0; valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
		while (valid)
		{
		Category *catitem;

			gtk_tree_model_get (model, &iter,
				LST_DEFCAT_DATAS, &catitem,
				-1);

			if(catitem->flt_select == TRUE)
				gtk_tree_store_set (GTK_TREE_STORE (model), &iter, LST_DEFCAT_TOGGLE, TRUE, -1);

			n_child = gtk_tree_model_iter_n_children (GTK_TREE_MODEL(model), &iter);
			gtk_tree_model_iter_children (GTK_TREE_MODEL(model), &child, &iter);
			while(n_child > 0)
			{
				i++;

				gtk_tree_model_get (model, &child,
					LST_DEFCAT_DATAS, &catitem,
					-1);

				if(catitem->flt_select == TRUE)
					gtk_tree_store_set (GTK_TREE_STORE (model), &child, LST_DEFCAT_TOGGLE, TRUE, -1);

				n_child--;
				gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &child);
			}

			/* Make iter point to the next row in the list store */
			i++; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
		}


	}
}


static void ui_flt_manage_cat_select(GtkWidget *widget, gpointer user_data)
{
struct ui_flt_manage_data *data;
gint select = GPOINTER_TO_INT(user_data);
GtkTreeModel *model;
GtkTreeIter	iter, child;
gboolean valid;
gint n_child;
gboolean toggle;

	DB( g_print("(ui_flt_manage) pay select\n") );

	data = g_object_get_data(G_OBJECT(gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW)), "inst_data");

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(data->LV_cat));
	valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	while (valid)
	{
		switch(select)
		{
			case BUTTON_ALL:
				gtk_tree_store_set (GTK_TREE_STORE (model), &iter, LST_DEFCAT_TOGGLE, TRUE, -1);
				break;
			case BUTTON_NONE:
				gtk_tree_store_set (GTK_TREE_STORE (model), &iter, LST_DEFCAT_TOGGLE, FALSE, -1);
				break;
			case BUTTON_INVERT:
					gtk_tree_model_get (model, &iter, LST_DEFCAT_TOGGLE, &toggle, -1);
					toggle ^= 1;
					gtk_tree_store_set (GTK_TREE_STORE (model), &iter, LST_DEFCAT_TOGGLE, toggle, -1);
				break;
		}

		n_child = gtk_tree_model_iter_n_children (GTK_TREE_MODEL(model), &iter);
		gtk_tree_model_iter_children (GTK_TREE_MODEL(model), &child, &iter);
		while(n_child > 0)
		{

			switch(select)
			{
				case BUTTON_ALL:
					gtk_tree_store_set (GTK_TREE_STORE (model), &child, LST_DEFCAT_TOGGLE, TRUE, -1);
					break;
				case BUTTON_NONE:
					gtk_tree_store_set (GTK_TREE_STORE (model), &child, LST_DEFCAT_TOGGLE, FALSE, -1);
					break;
				case BUTTON_INVERT:
						gtk_tree_model_get (model, &child, LST_DEFCAT_TOGGLE, &toggle, -1);
						toggle ^= 1;
						gtk_tree_store_set (GTK_TREE_STORE (model), &child, LST_DEFCAT_TOGGLE, toggle, -1);
					break;
			}

			n_child--;
			gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &child);
		}

		valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}

}



static gboolean
ui_flt_hub_category_activate_link (GtkWidget   *label,
               const gchar *uri,
               gpointer     data)
{
	DB( g_print(" comboboxlink '%s' \n", uri) );

	if (g_strcmp0 (uri, "all") == 0)	
	{
		ui_flt_manage_cat_select(label, GINT_TO_POINTER(BUTTON_ALL) );
	}
	else
	if (g_strcmp0 (uri, "non") == 0)	
	{
		ui_flt_manage_cat_select(label, GINT_TO_POINTER(BUTTON_NONE) );
	}
	else
	if (g_strcmp0 (uri, "inv") == 0)	
	{
		ui_flt_manage_cat_select(label, GINT_TO_POINTER(BUTTON_INVERT) );
	}

    return TRUE;
}


static GtkWidget *
ui_flt_hub_category_new (struct ui_flt_manage_data *data)
{
GtkWidget *hubbox, *scrollwin, *hbox, *vbox, *widget, *label, *tbar;
GtkToolItem *toolitem;
	
	hubbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, SPACING_SMALL);

	/*label = make_label (_("Categories"), 0, 0);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_LARGE, -1);
	gtk_box_pack_start (GTK_BOX (hubbox), label, FALSE, FALSE, 0);*/	

	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, SPACING_SMALL);
	gtk_box_pack_start (GTK_BOX (hubbox), hbox, FALSE, FALSE, 0);

	label = make_label (_("Select:"), 0, 0.5);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_SMALL, -1);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	label = make_clicklabel("all", _("All"));
	data->BT_cat[BUTTON_ALL] = label;
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	g_signal_connect (label, "activate-link", G_CALLBACK (ui_flt_hub_category_activate_link), NULL);
	
	label = make_clicklabel("non", _("None"));
	data->BT_cat[BUTTON_NONE] = label;
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	g_signal_connect (label, "activate-link", G_CALLBACK (ui_flt_hub_category_activate_link), NULL);

	label = make_clicklabel("inv", _("Invert"));
	data->BT_cat[BUTTON_INVERT] = label;
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	g_signal_connect (label, "activate-link", G_CALLBACK (ui_flt_hub_category_activate_link), NULL);

	//list
	vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
	gtk_box_pack_start (GTK_BOX (hubbox), vbox, TRUE, TRUE, 0);

 	scrollwin = gtk_scrolled_window_new(NULL,NULL);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrollwin), GTK_SHADOW_ETCHED_IN);
	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	//gtk_scrolled_window_set_min_content_height(GTK_SCROLLED_WINDOW(scrollwin), HB_MINHEIGHT_LIST);
	data->LV_cat = (GtkWidget *)ui_cat_listview_new(TRUE, FALSE);
	gtk_container_add(GTK_CONTAINER(scrollwin), data->LV_cat);
	gtk_widget_set_hexpand (scrollwin, TRUE);
	gtk_widget_set_vexpand (scrollwin, TRUE);
	gtk_box_pack_start (GTK_BOX (vbox), scrollwin, TRUE, TRUE, 0);

	//list toolbar
	tbar = gtk_toolbar_new();
	gtk_toolbar_set_icon_size (GTK_TOOLBAR(tbar), GTK_ICON_SIZE_MENU);
	gtk_toolbar_set_style(GTK_TOOLBAR(tbar), GTK_TOOLBAR_ICONS);
	gtk_style_context_add_class (gtk_widget_get_style_context (tbar), GTK_STYLE_CLASS_INLINE_TOOLBAR);
	gtk_box_pack_start (GTK_BOX (vbox), tbar, FALSE, FALSE, 0);

	toolitem = gtk_separator_tool_item_new ();
	gtk_tool_item_set_expand (toolitem, TRUE);
	gtk_separator_tool_item_set_draw(GTK_SEPARATOR_TOOL_ITEM(toolitem), FALSE);
	gtk_toolbar_insert(GTK_TOOLBAR(tbar), GTK_TOOL_ITEM(toolitem), -1);

	hbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
	toolitem = gtk_tool_item_new();
	gtk_container_add (GTK_CONTAINER(toolitem), hbox);
	gtk_toolbar_insert(GTK_TOOLBAR(tbar), GTK_TOOL_ITEM(toolitem), -1);
	
		widget = make_image_button(ICONNAME_HB_BUTTON_EXPAND, _("Expand all"));
		data->BT_expand = widget;
		gtk_box_pack_start (GTK_BOX (hbox), widget, FALSE, FALSE, 0);

		widget = make_image_button(ICONNAME_HB_BUTTON_COLLAPSE, _("Collapse all"));
		data->BT_collapse = widget;
		gtk_box_pack_start (GTK_BOX (hbox), widget, FALSE, FALSE, 0);

	g_signal_connect (G_OBJECT (data->BT_expand), "clicked", G_CALLBACK (ui_flt_hub_category_expand_all), NULL);
	g_signal_connect (G_OBJECT (data->BT_collapse), "clicked", G_CALLBACK (ui_flt_hub_category_collapse_all), NULL);


	return(hubbox);
}


/* = = = = = = = = = = = = = = = = */


static void ui_flt_hub_payee_get(Filter *flt, struct ui_flt_manage_data *data)
{
GtkTreeModel *model;
GtkTreeIter	iter;
gboolean valid;
gboolean toggled;
gint i;

	DB( g_print("(ui_flt_hub_payee) get\n") );

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(data->LV_pay));
	//selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(data->LV_pay));
	i=0; valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	while (valid)
	{
	Payee *payitem;

		gtk_tree_model_get (model, &iter,
			LST_DEFPAY_TOGGLE, &toggled,
			LST_DEFPAY_DATAS, &payitem,
			-1);

		DB( g_print(" payee k:%3d = %d (%s)\n", payitem->key, toggled, payitem->name) );
		da_flt_status_pay_set(flt, payitem->key, toggled);
		
		//payitem->flt_select = toggled;

		/* Make iter point to the next row in the list store */
		i++; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}

}


static void ui_flt_hub_payee_set(Filter *flt, struct ui_flt_manage_data *data)
{

	DB( g_print("(ui_flt_hub_payee) set\n") );

	if(data->filter != NULL)
	{
	GtkTreeModel *model;
	//GtkTreeSelection *selection;
	GtkTreeIter	iter;
	gboolean valid;
	gint i;

		model = gtk_tree_view_get_model(GTK_TREE_VIEW(data->LV_pay));
		//selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(data->LV_pay));
		i=0; valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
		while (valid)
		{
		Payee *payitem;
		gboolean status;

			gtk_tree_model_get (model, &iter,
				LST_DEFPAY_DATAS, &payitem,
				-1);

			status = da_flt_status_pay_get(flt, payitem->key);
			gtk_list_store_set (GTK_LIST_STORE (model), &iter, LST_DEFPAY_TOGGLE, status, -1);

			/* Make iter point to the next row in the list store */
			i++; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
		}


	}
}


static void ui_flt_manage_pay_select(GtkWidget *widget, gpointer user_data)
{
struct ui_flt_manage_data *data;
gint select = GPOINTER_TO_INT(user_data);
GtkTreeModel *model;
GtkTreeIter	iter;
gboolean valid;
gboolean toggle;

	DB( g_print("(ui_flt_manage) pay select\n") );

	data = g_object_get_data(G_OBJECT(gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW)), "inst_data");

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(data->LV_pay));
	valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	while (valid)
	{
		switch(select)
		{
			case BUTTON_ALL:
				gtk_list_store_set (GTK_LIST_STORE (model), &iter, LST_DEFPAY_TOGGLE, TRUE, -1);
				break;
			case BUTTON_NONE:
				gtk_list_store_set (GTK_LIST_STORE (model), &iter, LST_DEFPAY_TOGGLE, FALSE, -1);
				break;
			case BUTTON_INVERT:
					gtk_tree_model_get (model, &iter, LST_DEFPAY_TOGGLE, &toggle, -1);
					toggle ^= 1;
					gtk_list_store_set (GTK_LIST_STORE (model), &iter, LST_DEFPAY_TOGGLE, toggle, -1);
				break;
		}
		valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
}


static gboolean
ui_flt_hub_payee_activate_link (GtkWidget   *label,
               const gchar *uri,
               gpointer     data)
{
	DB( g_print(" comboboxlink '%s' \n", uri) );

	if (g_strcmp0 (uri, "all") == 0)	
	{
		ui_flt_manage_pay_select(label, GINT_TO_POINTER(BUTTON_ALL) );
	}
	else
	if (g_strcmp0 (uri, "non") == 0)	
	{
		ui_flt_manage_pay_select(label, GINT_TO_POINTER(BUTTON_NONE) );
	}
	else
	if (g_strcmp0 (uri, "inv") == 0)	
	{
		ui_flt_manage_pay_select(label, GINT_TO_POINTER(BUTTON_INVERT) );
	}

    return TRUE;
}


static GtkWidget *
ui_flt_hub_payee_new (struct ui_flt_manage_data *data)
{
GtkWidget *scrollwin, *hbox, *vbox, *label;

	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, SPACING_SMALL);

	/*label = make_label (_("Payees"), 0, 0);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_LARGE, -1);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);*/	

	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, SPACING_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	label = make_label (_("Select:"), 0, 0.5);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_SMALL, -1);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	label = make_clicklabel("all", _("All"));
	data->BT_pay[BUTTON_ALL] = label;
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	g_signal_connect (label, "activate-link", G_CALLBACK (ui_flt_hub_payee_activate_link), NULL);
	
	label = make_clicklabel("non", _("None"));
	data->BT_pay[BUTTON_NONE] = label;
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	g_signal_connect (label, "activate-link", G_CALLBACK (ui_flt_hub_payee_activate_link), NULL);

	label = make_clicklabel("inv", _("Invert"));
	data->BT_pay[BUTTON_INVERT] = label;
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	g_signal_connect (label, "activate-link", G_CALLBACK (ui_flt_hub_payee_activate_link), NULL);

 	scrollwin = gtk_scrolled_window_new(NULL,NULL);
	gtk_box_pack_start (GTK_BOX (vbox), scrollwin, TRUE, TRUE, 0);

	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrollwin), GTK_SHADOW_ETCHED_IN);
	//gtk_container_set_border_width (GTK_CONTAINER(scrollwin), SPACING_SMALL);

	data->LV_pay = (GtkWidget *)ui_pay_listview_new(TRUE, FALSE);
	gtk_container_add(GTK_CONTAINER(scrollwin), data->LV_pay);

	return(vbox);
}


/* = = = = = = = = = = = = = = = = */


static void ui_flt_hub_account_get(Filter *flt, struct ui_flt_hub_stuf *data)
{
GtkTreeModel *model;
GtkTreeIter	iter;
gboolean valid;
gint i;

	DB( g_print("(ui_flt_hub_account) get\n") );

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(data->treeview));
	//selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(data->LV_acc));
	i=0; valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	while (valid)
	{
	Account *accitem;
	gboolean toggled;

		gtk_tree_model_get (model, &iter,
			LST_DEFACC_TOGGLE, &toggled,
			LST_DEFACC_DATAS, &accitem,
			-1);

		DB( g_print(" acc k:%3d = %d (%s)\n", accitem->key, toggled, accitem->name) );
		da_flt_status_acc_set(flt, accitem->key, toggled);

		//accitem->flt_select = toggled;

		/* Make iter point to the next row in the list store */
		i++; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}
}


static void ui_flt_hub_account_set(Filter *flt, struct ui_flt_hub_stuf *data)
{
GtkTreeModel *model;
GtkTreeIter	iter;
gboolean valid;
gint i;

	DB( g_print("(ui_flt_hub_account) set\n") );

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(data->treeview));
	//selection = gtk_tree_view_get_selection(GTK_TREE_VIEW(data->LV_acc));
	i=0; valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
	while (valid)
	{
	Account *accitem;
	gboolean status;

		gtk_tree_model_get (model, &iter,
			LST_DEFACC_DATAS, &accitem,
			-1);

		status = da_flt_status_acc_get(flt, accitem->key);
			gtk_list_store_set (GTK_LIST_STORE (model), &iter,
				LST_DEFACC_TOGGLE, status, -1);

		/* Make iter point to the next row in the list store */
		i++; valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
	}


}


static gboolean
ui_flt_hub_account_activate_link (GtkWidget   *label,
               const gchar *uri,
               gpointer     user_data)
{
GtkTreeModel *model;
GtkTreeIter	iter;
gboolean valid;
gboolean toggle;

	DB( g_print("(ui_flt_hub_account) activate_link\n") );

	g_return_val_if_fail(GTK_IS_TREE_VIEW(user_data), TRUE);

	model = gtk_tree_view_get_model(GTK_TREE_VIEW(user_data));

	DB( g_print(" comboboxlink '%s' \n", uri) );

	if (g_strcmp0 (uri, "all") == 0)	
	{
		valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
		while (valid)
		{
			gtk_list_store_set (GTK_LIST_STORE (model), &iter, LST_DEFACC_TOGGLE, TRUE, -1);
			valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
		}
	}
	else
	if (g_strcmp0 (uri, "non") == 0)	
	{
		valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
		while (valid)
		{
			gtk_list_store_set (GTK_LIST_STORE (model), &iter, LST_DEFACC_TOGGLE, FALSE, -1);
			valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
		}
	}
	else
	if (g_strcmp0 (uri, "inv") == 0)	
	{
		valid = gtk_tree_model_get_iter_first(GTK_TREE_MODEL(model), &iter);
		while (valid)
		{
			gtk_tree_model_get (model, &iter, LST_DEFACC_TOGGLE, &toggle, -1);
			toggle ^= 1;
			gtk_list_store_set (GTK_LIST_STORE (model), &iter, LST_DEFACC_TOGGLE, toggle, -1);
			valid = gtk_tree_model_iter_next(GTK_TREE_MODEL(model), &iter);
		}
	}

    return TRUE;
}


static GtkWidget *
ui_flt_hub_account_new (struct ui_flt_hub_stuf *data)
{
GtkWidget *scrollwin, *hbox, *vbox, *label;

	DB( g_print("(ui_flt_hub_account) new\n") );

	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, SPACING_SMALL);

	/*label = make_label (_("Accounts"), 0, 0);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_LARGE, -1);
	gtk_box_pack_start (GTK_BOX (vbox), label, FALSE, FALSE, 0);*/

	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, SPACING_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

	label = make_label (_("Select:"), 0, 0.5);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_SMALL, -1);
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	label = make_clicklabel("all", _("All"));
	data->BT_all= label;
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
	
	label = make_clicklabel("non", _("None"));
	data->BT_none = label;
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

	label = make_clicklabel("inv", _("Invert"));
	data->BT_invert = label;
	gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

 	scrollwin = gtk_scrolled_window_new(NULL,NULL);
	//gtk_widget_set_size_request (scrollwin, HB_MINWIDTH_LIST, HB_MINHEIGHT_LIST);

	gtk_box_pack_start (GTK_BOX (vbox), scrollwin, TRUE, TRUE, 0);

	gtk_scrolled_window_set_policy(GTK_SCROLLED_WINDOW(scrollwin), GTK_POLICY_NEVER, GTK_POLICY_AUTOMATIC);
	gtk_scrolled_window_set_shadow_type (GTK_SCROLLED_WINDOW (scrollwin), GTK_SHADOW_ETCHED_IN);
	//gtk_container_set_border_width (GTK_CONTAINER(scrollwin), SPACING_SMALL);

	data->treeview = (GtkWidget *)ui_acc_listview_new(TRUE);
	gtk_container_add(GTK_CONTAINER(scrollwin), data->treeview);

	g_signal_connect (data->BT_all, "activate-link", G_CALLBACK (ui_flt_hub_account_activate_link), data->treeview);
	g_signal_connect (data->BT_none, "activate-link", G_CALLBACK (ui_flt_hub_account_activate_link), data->treeview);
	g_signal_connect (data->BT_invert, "activate-link", G_CALLBACK (ui_flt_hub_account_activate_link), data->treeview);

	return(vbox);
}

/* = = = = = = = = = = = = = = = = */







/*
**
*/
static void ui_flt_manage_option_update(GtkWidget *widget, gpointer user_data)
{
struct ui_flt_manage_data *data;
gint active;
gboolean sensitive;

	DB( g_print("(ui_flt_manage) update\n") );

	data = g_object_get_data(G_OBJECT(gtk_widget_get_ancestor(widget, GTK_TYPE_WINDOW)), "inst_data");

	// status
	active = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[FLT_GRP_STATUS]));
	sensitive = active == 0 ? FALSE : TRUE;
	gtk_widget_set_sensitive(data->GR_page[FLT_GRP_STATUS], sensitive);

	// type
	active = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[FLT_GRP_TYPE]));
	sensitive = active == 0 ? FALSE : TRUE;
	gtk_widget_set_sensitive(data->GR_page[FLT_GRP_TYPE], sensitive);
	
	// date
	active = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[FLT_GRP_DATE]));
	sensitive = active == 0 ? FALSE : TRUE;
	gtk_widget_set_sensitive(data->GR_page[FLT_GRP_DATE], sensitive);

	// amount
	active = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[FLT_GRP_AMOUNT]));
	sensitive = active == 0 ? FALSE : TRUE;
	gtk_widget_set_sensitive(data->GR_page[FLT_GRP_AMOUNT], sensitive);

	// text
	active = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[FLT_GRP_TEXT]));
	sensitive = active == 0 ? FALSE : TRUE;
	gtk_widget_set_sensitive(data->GR_page[FLT_GRP_TEXT], sensitive);

	// paymode
	active = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[FLT_GRP_PAYMODE]));
	sensitive = active == 0 ? FALSE : TRUE;
	gtk_widget_set_sensitive(data->GR_page[FLT_GRP_PAYMODE], sensitive);

	// account
	if(data->show_account == TRUE)
	{
		active = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[FLT_GRP_ACCOUNT]));
		sensitive = active == 0 ? FALSE : TRUE;
		gtk_widget_set_sensitive(data->GR_page[FLT_GRP_ACCOUNT], sensitive);
	}

	// payee
	active = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[FLT_GRP_PAYEE]));
	sensitive = active == 0 ? FALSE : TRUE;
	gtk_widget_set_sensitive(data->GR_page[FLT_GRP_PAYEE], sensitive);

	// category
	active = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[FLT_GRP_CATEGORY]));
	sensitive = active == 0 ? FALSE : TRUE;
	gtk_widget_set_sensitive(data->GR_page[FLT_GRP_CATEGORY], sensitive);

}


/*
**
*/
static void ui_flt_manage_get(struct ui_flt_manage_data *data)
{
gint i;
gchar *txt;

	DB( g_print("(ui_flt_manage) get\n") );

	if(data->filter !=NULL)
	{

		for(i=0;i<FLT_GRP_MAX;i++)
		{
			if(data->show_account == FALSE && i == FLT_GRP_ACCOUNT)
				continue;

			data->filter->option[i] = gtk_combo_box_get_active(GTK_COMBO_BOX(data->CY_option[i]));
		}

	//date
		DB( g_print(" date\n") );
		data->filter->mindate = gtk_date_entry_get_date(GTK_DATE_ENTRY(data->PO_mindate));
		data->filter->maxdate = gtk_date_entry_get_date(GTK_DATE_ENTRY(data->PO_maxdate));

	//status
		DB( g_print(" status/type\n") );
		data->filter->type = hbtk_combo_box_get_active_id(GTK_COMBO_BOX_TEXT(data->CY_type));
		data->filter->status = hbtk_combo_box_get_active_id(GTK_COMBO_BOX_TEXT(data->CY_status));

		data->filter->forceadd = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data->CM_forceadd));
		data->filter->forcechg = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data->CM_forcechg));
		data->filter->forceremind  = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data->CM_forceremind));
		data->filter->forcevoid  = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data->CM_forcevoid));

	//paymode
		DB( g_print(" paymode\n") );
		for(i=0;i<NUM_PAYMODE_MAX;i++)
		{
		gint uid;
			
			if( !GTK_IS_TOGGLE_BUTTON(data->CM_paymode[i] ))
				continue;

			uid = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(data->CM_paymode[i]), "uid")); 
			data->filter->paymode[uid] = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data->CM_paymode[i]));
		}
			
	//amount
		data->filter->minamount = gtk_spin_button_get_value(GTK_SPIN_BUTTON(data->ST_minamount));
		data->filter->maxamount = gtk_spin_button_get_value(GTK_SPIN_BUTTON(data->ST_maxamount));

	//text:memo
		data->filter->exact  = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data->CM_exact));
		//free any previous string
		if(	data->filter->memo )
		{
			g_free(data->filter->memo);
			data->filter->memo = NULL;
		}
		txt = (gchar *)gtk_entry_get_text(GTK_ENTRY(data->ST_memo));

		if (txt && *txt)	// ignore if entry is empty
		{
			data->filter->memo = g_strdup(txt);
		}

	//text:info
		//free any previous string
		if(	data->filter->info )
		{
			g_free(data->filter->info);
			data->filter->info = NULL;
		}
		txt = (gchar *)gtk_entry_get_text(GTK_ENTRY(data->ST_info));
		// ignore if entry is empty
		if (txt && *txt)
		{
			data->filter->info = g_strdup(txt);
		}

	//text:tag
		//free any previous string
		if(	data->filter->tag )
		{
			g_free(data->filter->tag);
			data->filter->tag = NULL;
		}
		txt = (gchar *)gtk_entry_get_text(GTK_ENTRY(data->ST_tag));
		// ignore if entry is empty
		if (txt && *txt)
		{
			data->filter->tag = g_strdup(txt);
		}

	// account
		if(data->show_account == TRUE)
		{
			ui_flt_hub_account_get(data->filter, &data->pnl_acc_data);
		}

	// payee
		ui_flt_hub_payee_get(data->filter, data);

	// category
		ui_flt_hub_category_get(data->filter, data);

	// active tab
	g_strlcpy(data->filter->last_tab, gtk_stack_get_visible_child_name(GTK_STACK(data->stack)), 8);
	DB( g_print(" page is '%s'\n", data->filter->last_tab) );
	

	}
}


/*
**
*/
static void ui_flt_manage_set(struct ui_flt_manage_data *data)
{

	DB( g_print("(ui_flt_manage) set\n") );

	if(data->filter != NULL)
	{
	gint i;

		DB( g_print(" options\n") );

		for(i=0;i<FLT_GRP_MAX;i++)
		{
			if(data->show_account == FALSE && i == FLT_GRP_ACCOUNT)
				continue;

			gtk_combo_box_set_active(GTK_COMBO_BOX(data->CY_option[i]), data->filter->option[i]);
		}

		//DB( g_print(" setdate %d to %x\n", data->filter->mindate, data->PO_mindate) );
		//DB( g_print(" setdate %d to %x\n", 0, data->PO_mindate) );
	//date
		DB( g_print(" date\n") );
		gtk_date_entry_set_date(GTK_DATE_ENTRY(data->PO_mindate), data->filter->mindate);
		gtk_date_entry_set_date(GTK_DATE_ENTRY(data->PO_maxdate), data->filter->maxdate);

	//status
		DB( g_print(" status/type\n") );
		hbtk_combo_box_set_active_id(GTK_COMBO_BOX_TEXT(data->CY_type), data->filter->type);
		hbtk_combo_box_set_active_id(GTK_COMBO_BOX_TEXT(data->CY_status), data->filter->status);

		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->CM_forceadd), data->filter->forceadd);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->CM_forcechg), data->filter->forcechg);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->CM_forceremind), data->filter->forceremind);
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->CM_forcevoid), data->filter->forcevoid);

	//paymode
		DB( g_print(" paymode\n") );

		for(i=0;i<NUM_PAYMODE_MAX;i++)
		{
		gint uid;
			
			if( !GTK_IS_TOGGLE_BUTTON(data->CM_paymode[i] ))
				continue;

			uid = GPOINTER_TO_UINT(g_object_get_data(G_OBJECT(data->CM_paymode[i]), "uid")); 
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->CM_paymode[i]), data->filter->paymode[uid]);

		}

	//amount
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(data->ST_minamount), data->filter->minamount);
		gtk_spin_button_set_value(GTK_SPIN_BUTTON(data->ST_maxamount), data->filter->maxamount);

		//text
		gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->CM_exact), data->filter->exact);
		gtk_entry_set_text(GTK_ENTRY(data->ST_info), (data->filter->info != NULL) ? data->filter->info : "");
		gtk_entry_set_text(GTK_ENTRY(data->ST_memo), (data->filter->memo != NULL) ? data->filter->memo : "");
		gtk_entry_set_text(GTK_ENTRY(data->ST_tag), (data->filter->tag != NULL) ? data->filter->tag : "");

	//account
		if(data->show_account == TRUE)
		{
			DB( g_print(" account\n") );
			ui_flt_hub_account_set(data->filter, &data->pnl_acc_data);
		}

	// payee
		ui_flt_hub_payee_set(data->filter, data);

	// category
		ui_flt_hub_category_set(data->filter, data);

	}
}


/*
**
*/
static void ui_flt_manage_setup(struct ui_flt_manage_data *data)
{

	DB( g_print("(ui_flt_manage) setup\n") );

	if(data->show_account == TRUE && data->pnl_acc_data.treeview != NULL)
	{
		//gtk_tree_selection_set_mode(GTK_TREE_SELECTION(gtk_tree_view_get_selection(GTK_TREE_VIEW(data->LV_acc))), GTK_SELECTION_MULTIPLE);

		ui_acc_listview_populate(data->pnl_acc_data.treeview, ACC_LST_INSERT_REPORT);
		//populate_view_acc(data->LV_acc, GLOBALS->acc_list, FALSE);
	}

	if(data->LV_pay)
	{
		//gtk_tree_selection_set_mode(GTK_TREE_SELECTION(gtk_tree_view_get_selection(GTK_TREE_VIEW(data->LV_pay))), GTK_SELECTION_MULTIPLE);

		ui_pay_listview_populate(data->LV_pay, NULL);
		//populate_view_pay(data->LV_pay, GLOBALS->pay_list, FALSE);
	}

	if(data->LV_cat)
	{
		//gtk_tree_selection_set_mode(GTK_TREE_SELECTION(gtk_tree_view_get_selection(GTK_TREE_VIEW(data->LV_cat))), GTK_SELECTION_MULTIPLE);

		//populate_view_cat(data->LV_cat, GLOBALS->cat_list, FALSE);
		ui_cat_listview_populate(data->LV_cat, CAT_TYPE_ALL);
		gtk_tree_view_expand_all (GTK_TREE_VIEW(data->LV_cat));
	}
}


static GtkWidget *ui_flt_manage_page_option (struct ui_flt_manage_data *data, guint index)
{
GtkWidget *container, *hbox, *label;

	container = gtk_box_new(GTK_ORIENTATION_VERTICAL, SPACING_LARGE);
	//gtk_container_set_border_width (GTK_CONTAINER (container), SPACING_MEDIUM);

	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, SPACING_SMALL);
	gtk_box_pack_start (GTK_BOX (container), hbox, FALSE, FALSE, 0);
	
	label = make_label_widget(_("_Option:"));
	gtk_box_pack_start (GTK_BOX (hbox), label, TRUE, TRUE, 0);
	data->CY_option[index] = make_nainex(label);
	//data->CY_option[index] = hbtk_radio_button_new(nainex_label_names, TRUE);
	gtk_box_pack_start (GTK_BOX (hbox), data->CY_option[index], TRUE, TRUE, 0);

	return(container);
}


static GtkWidget *ui_flt_manage_page_category (struct ui_flt_manage_data *data)
{
GtkWidget *container, *panel;

	container = ui_flt_manage_page_option(data, FLT_GRP_CATEGORY);
	panel = ui_flt_hub_category_new(data);
	data->GR_page[FLT_GRP_CATEGORY] = panel;
	gtk_box_pack_start (GTK_BOX (container), panel, TRUE, TRUE, 0);

	return(container);
}


static GtkWidget *ui_flt_manage_page_payee (struct ui_flt_manage_data *data)
{
GtkWidget *container, *panel;

	container = ui_flt_manage_page_option(data, FLT_GRP_PAYEE);
	panel = ui_flt_hub_payee_new(data);
	data->GR_page[FLT_GRP_PAYEE] = panel;
	gtk_box_pack_start (GTK_BOX (container), panel, TRUE, TRUE, 0);

	return(container);
}

/*
** account filter
*/
static GtkWidget *ui_flt_manage_page_account (struct ui_flt_manage_data *data)
{
GtkWidget *container, *panel;

	container = ui_flt_manage_page_option(data, FLT_GRP_ACCOUNT);
	panel = ui_flt_hub_account_new(&data->pnl_acc_data);
	data->GR_page[FLT_GRP_ACCOUNT] = panel;
	gtk_box_pack_start (GTK_BOX (container), panel, TRUE, TRUE, 0);

	return(container);
}


static GtkWidget *ui_flt_manage_part_date(struct ui_flt_manage_data *data)
{
GtkWidget *container, *table, *label;
gint row;

	container = ui_flt_manage_page_option(data, FLT_GRP_DATE);

    table = gtk_grid_new ();
	gtk_grid_set_row_spacing (GTK_GRID (table), SPACING_SMALL);
	gtk_grid_set_column_spacing (GTK_GRID (table), SPACING_MEDIUM);
	
	data->GR_page[FLT_GRP_DATE] = table;
	gtk_box_pack_start (GTK_BOX (container), table, TRUE, TRUE, 0);
	
	row = 0;
	/*label = make_label (_("Dates"), 0, 0);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_LARGE, -1);
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 3, 1);

	row++;*/
	label = make_label_widget(_("_From:"));
	gtk_grid_attach (GTK_GRID (table), label, 1, row, 1, 1);
	data->PO_mindate = gtk_date_entry_new(label);
	gtk_grid_attach (GTK_GRID (table), data->PO_mindate, 2, row, 1, 1);

	row++;
	label = make_label_widget(_("_To:"));
	gtk_grid_attach (GTK_GRID (table), label, 1, row, 1, 1);
	data->PO_maxdate = gtk_date_entry_new(label);
	gtk_grid_attach (GTK_GRID (table), data->PO_maxdate, 2, row, 1, 1);

	return container;
}


static GtkWidget *ui_flt_manage_part_text(struct ui_flt_manage_data *data)
{
GtkWidget *container, *table, *label;
gint row;

	container = ui_flt_manage_page_option(data, FLT_GRP_TEXT);
	
	table = gtk_grid_new ();
	gtk_grid_set_row_spacing (GTK_GRID (table), SPACING_SMALL);
	gtk_grid_set_column_spacing (GTK_GRID (table), SPACING_MEDIUM);

	data->GR_page[FLT_GRP_TEXT] = table;
	gtk_box_pack_start (GTK_BOX (container), table, TRUE, TRUE, 0);
	
	row = 0;
	/*label = make_label (_("Texts"), 0, 0);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_LARGE, -1);
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 3, 1);

	row++;*/
	label = make_label_widget(_("_Memo:"));
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 1, 1);
	data->ST_memo = make_string(label);
	gtk_widget_set_hexpand (data->ST_memo, TRUE);
	gtk_grid_attach (GTK_GRID (table), data->ST_memo, 1, row, 2, 1);

	row++;
	label = make_label_widget(_("_Info:"));
	//----------------------------------------- l, r, t, b
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 1, 1);
	data->ST_info = make_string(label);
	gtk_widget_set_hexpand (data->ST_info, TRUE);
	gtk_grid_attach (GTK_GRID (table), data->ST_info, 1, row, 2, 1);

	row++;
	label = make_label_widget(_("_Tag:"));
	//----------------------------------------- l, r, t, b
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 1, 1);
	data->ST_tag = make_string(label);
	gtk_widget_set_hexpand (data->ST_tag, TRUE);
	gtk_grid_attach (GTK_GRID (table), data->ST_tag, 1, row, 2, 1);

	row++;
	data->CM_exact = gtk_check_button_new_with_mnemonic (_("Case _sensitive"));
	gtk_grid_attach (GTK_GRID (table), data->CM_exact, 1, row, 2, 1);


	return container;
}

static GtkWidget *ui_flt_manage_part_amount(struct ui_flt_manage_data *data)
{
GtkWidget *container, *table, *label;
gint row;

	container = ui_flt_manage_page_option(data, FLT_GRP_AMOUNT);
	
	table = gtk_grid_new ();
	gtk_grid_set_row_spacing (GTK_GRID (table), SPACING_SMALL);
	gtk_grid_set_column_spacing (GTK_GRID (table), SPACING_MEDIUM);

	data->GR_page[FLT_GRP_AMOUNT] = table;
	gtk_box_pack_start (GTK_BOX (container), table, TRUE, TRUE, 0);
	
	row = 0;
	/*label = make_label (_("Amounts"), 0, 0);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_LARGE, -1);
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 3, 1);

	row++;*/
	label = make_label_widget(_("_From:"));
	//----------------------------------------- l, r, t, b
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 1, 1);
	data->ST_minamount = make_amount(label);
	gtk_grid_attach (GTK_GRID (table), data->ST_minamount, 1, row, 2, 1);

	row++;
	label = make_label_widget(_("_To:"));
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 1, 1);
	data->ST_maxamount = make_amount(label);
	gtk_grid_attach (GTK_GRID (table), data->ST_maxamount, 1, row, 2, 1);

	return container;
}


static GtkWidget *ui_flt_manage_part_status(struct ui_flt_manage_data *data)
{
GtkWidget *content, *container, *table, *label, *widget, *vbox;
gint row;

	content = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);

	//status
	container = ui_flt_manage_page_option(data, FLT_GRP_STATUS);
	gtk_box_pack_start (GTK_BOX (content), container, TRUE, TRUE, 0);

	table = gtk_grid_new ();
	gtk_grid_set_row_spacing (GTK_GRID (table), SPACING_SMALL);
	gtk_grid_set_column_spacing (GTK_GRID (table), SPACING_MEDIUM);
	gtk_widget_set_margin_bottom(table, SPACING_LARGE);

	data->GR_page[FLT_GRP_STATUS] = table;
	gtk_box_pack_start (GTK_BOX (container), table, TRUE, TRUE, 0);

	row = 0;
	label = make_label_widget(_("_Status:"));
	//----------------------------------------- l, r, t, b
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 1, 1);
	
	widget = hbtk_combo_box_new(NULL);
	hbtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(widget), FLT_STATUS_ALL       , _("Any Status"));
	hbtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(widget), FLT_STATUS_CLEARED   , _("Cleared"));
	hbtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(widget), FLT_STATUS_RECONCILED, _("Reconciled"));
	data->CY_status = widget;
	gtk_grid_attach (GTK_GRID (table), widget, 1, row, 1, 1);
	
	// type
	container = ui_flt_manage_page_option(data, FLT_GRP_TYPE);
	gtk_box_pack_start (GTK_BOX (content), container, TRUE, TRUE, 0);

	table = gtk_grid_new ();
	gtk_grid_set_row_spacing (GTK_GRID (table), SPACING_SMALL);
	gtk_grid_set_column_spacing (GTK_GRID (table), SPACING_MEDIUM);

	data->GR_page[FLT_GRP_TYPE] = table;
	gtk_box_pack_start (GTK_BOX (container), table, FALSE, FALSE, 0);

	row = 0;
	label = make_label_widget(_("_Type:"));
	//----------------------------------------- l, r, t, b
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 1, 1);

	widget = hbtk_combo_box_new(NULL);
	hbtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(widget), FLT_TYPE_ALL    , _("Any Type"));
	hbtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(widget), FLT_TYPE_EXPENSE, _("Expense"));
	hbtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(widget), FLT_TYPE_INCOME , _("Income"));
	hbtk_combo_box_text_append(GTK_COMBO_BOX_TEXT(widget), FLT_TYPE_INTXFER, _("Transfer"));
	data->CY_type = widget;
	gtk_grid_attach (GTK_GRID (table), widget, 1, row, 1, 1);


	table = gtk_grid_new ();
	gtk_grid_set_row_spacing (GTK_GRID (table), SPACING_SMALL);
	gtk_grid_set_column_spacing (GTK_GRID (table), SPACING_MEDIUM);

	gtk_box_pack_start (GTK_BOX (content), table, TRUE, TRUE, 0);

		row = 0;
		label = make_label_widget(_("Force:"));
		data->LB_force = label;
		gtk_grid_attach (GTK_GRID (table), label, 1, row, 1, 1);

		vbox = gtk_box_new (GTK_ORIENTATION_VERTICAL, 0);
		data->GR_force = vbox;
		gtk_grid_attach (GTK_GRID (table), vbox, 2, row, 1, 1);

		widget = gtk_check_button_new_with_mnemonic (_("display 'Added'"));
		data->CM_forceadd = widget;
		gtk_box_pack_start (GTK_BOX (vbox), widget, TRUE, TRUE, 0);

		widget = gtk_check_button_new_with_mnemonic (_("display 'Edited'"));
		data->CM_forcechg = widget;
		gtk_box_pack_start (GTK_BOX (vbox), widget, TRUE, TRUE, 0);

		widget = gtk_check_button_new_with_mnemonic (_("display 'Remind'"));
		data->CM_forceremind = widget;
		gtk_box_pack_start (GTK_BOX (vbox), widget, TRUE, TRUE, 0);

		widget = gtk_check_button_new_with_mnemonic (_("display 'Void'"));
		data->CM_forcevoid = widget;
		gtk_box_pack_start (GTK_BOX (vbox), widget, TRUE, TRUE, 0);

	return content;
}


static gboolean
ui_flt_manage_part_paymode_activate_link (GtkWidget   *label,
               const gchar *uri,
               gpointer     user_data)
{
struct ui_flt_manage_data *data;
gint i;
	
	DB( g_print("(ui_flt_hub_account) activate_link\n") );

	data = g_object_get_data(G_OBJECT(gtk_widget_get_ancestor(label, GTK_TYPE_WINDOW)), "inst_data");
	
	for(i=0;i<NUM_PAYMODE_MAX;i++)
	{
		if( !GTK_IS_TOGGLE_BUTTON(data->CM_paymode[i] ))
			continue;

		if (g_strcmp0 (uri, "all") == 0)	
		{
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->CM_paymode[i]), TRUE);
		}
		else
		if (g_strcmp0 (uri, "non") == 0)	
		{
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->CM_paymode[i]), FALSE);
		}
		else
		if (g_strcmp0 (uri, "inv") == 0)	
		{
		gboolean act = gtk_toggle_button_get_active(GTK_TOGGLE_BUTTON(data->CM_paymode[i]));
			gtk_toggle_button_set_active(GTK_TOGGLE_BUTTON(data->CM_paymode[i]), !act);
		}
	}

    return TRUE;
}



static GtkWidget *ui_flt_manage_part_paymode(struct ui_flt_manage_data *data)
{
GtkWidget *container, *vbox, *hbox, *label, *widget, *table, *image;
HbKivData *tmp, *kvdata = CYA_TXN_PAYMODE;
gint i, row;

	container = ui_flt_manage_page_option(data, FLT_GRP_PAYMODE);
	
	vbox = gtk_box_new(GTK_ORIENTATION_VERTICAL, SPACING_SMALL);
	data->GR_page[FLT_GRP_PAYMODE] = vbox;
	gtk_box_pack_start (GTK_BOX (container), vbox, TRUE, TRUE, 0);

	hbox = gtk_box_new(GTK_ORIENTATION_HORIZONTAL, SPACING_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox), hbox, FALSE, FALSE, 0);

		label = make_label (_("Select:"), 0, 0.5);
		gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_SMALL, -1);
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);

		label = make_clicklabel("all", _("All"));
		//data->BT_cat[BUTTON_ALL] = label;
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		g_signal_connect (label, "activate-link", G_CALLBACK (ui_flt_manage_part_paymode_activate_link), NULL);
		
		label = make_clicklabel("non", _("None"));
		//data->BT_cat[BUTTON_NONE] = label;
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		g_signal_connect (label, "activate-link", G_CALLBACK (ui_flt_manage_part_paymode_activate_link), NULL);

		label = make_clicklabel("inv", _("Invert"));
		//data->BT_cat[BUTTON_INVERT] = label;
		gtk_box_pack_start (GTK_BOX (hbox), label, FALSE, FALSE, 0);
		g_signal_connect (label, "activate-link", G_CALLBACK (ui_flt_manage_part_paymode_activate_link), NULL);

	/*label = make_label (_("Payments"), 0, 0);
	gimp_label_set_attributes (GTK_LABEL (label), PANGO_ATTR_SCALE, PANGO_SCALE_LARGE, -1);
	gtk_grid_attach (GTK_GRID (table), label, 0, row, 3, 1);

	row++;*/
	table = gtk_grid_new ();
	//gtk_grid_set_row_spacing (GTK_GRID (table), SPACING_SMALL);
	//gtk_grid_set_column_spacing (GTK_GRID (table), SPACING_SMALL);
	gtk_box_pack_start (GTK_BOX (vbox), table, FALSE, FALSE, 0);
	
	
	for(i=0;i<NUM_PAYMODE_MAX;i++)
	{
	tmp = &kvdata[i];

		if( tmp->name == NULL )
			break;

		row = i;

		image = gtk_image_new_from_icon_name( tmp->iconname, GTK_ICON_SIZE_MENU);
		gtk_grid_attach (GTK_GRID (table), image, 0, row, 1, 1);

		widget = gtk_check_button_new_with_mnemonic(_(tmp->name));
		data->CM_paymode[i] = widget;
		g_object_set_data(G_OBJECT(widget), "uid", GUINT_TO_POINTER(tmp->key));
		gtk_grid_attach (GTK_GRID (table), data->CM_paymode[i], 1, row, 1, 1);
	}

	return container;
}


/*
**
*/
gint ui_flt_manage_dialog_new(GtkWindow *parentwindow, Filter *filter, gboolean show_account, gboolean txnmode)
{
struct ui_flt_manage_data *data;
GtkWidget *dialog, *content, *mainbox, *sidebar, *stack, *page, *widget;
gint w, h, dw, dh;

	data = g_malloc0(sizeof(struct ui_flt_manage_data));

	data->filter = filter;

	dialog = gtk_dialog_new_with_buttons (_("Edit filter"),
			GTK_WINDOW (parentwindow),
			0,	//no flags
			NULL, //no buttons
			NULL);

	if(!txnmode)
	{
		widget = gtk_dialog_add_button(GTK_DIALOG(dialog), _("_Reset"),	55);
		gtk_widget_set_margin_right(widget, SPACING_MEDIUM);
	}

	gtk_dialog_add_button(GTK_DIALOG(dialog), _("_Cancel"),	GTK_RESPONSE_REJECT);
	gtk_dialog_add_button(GTK_DIALOG(dialog), _("_OK"),    GTK_RESPONSE_ACCEPT);


	//TODO:
	gtk_window_set_icon_name(GTK_WINDOW (dialog), ICONNAME_HB_FILTER);

	//set a nice dialog size
	gtk_window_get_size(GTK_WINDOW(GLOBALS->mainwindow), &w, &h);
	dh = (h*1.33/PHI);
	//ratio 2:3
	dw = (dh * 2) / 3;
	DB( g_print(" main w=%d h=%d => diag w=%d h=%d\n", w, h, dw, dh) );
	gtk_window_set_default_size (GTK_WINDOW(dialog), dw, dh);

	//store our window private data
	g_object_set_data(G_OBJECT(dialog), "inst_data", (gpointer)data);
	DB( g_print("(ui_flt_manage) window=%p, inst_data=%p\n", dialog, data) );

    g_signal_connect (dialog, "destroy",
			G_CALLBACK (gtk_widget_destroyed), &dialog);

	content = gtk_dialog_get_content_area(GTK_DIALOG (dialog));

	mainbox = gtk_box_new (GTK_ORIENTATION_HORIZONTAL, 0);
	gtk_box_pack_start (GTK_BOX (content), mainbox, TRUE, TRUE, 0);

	sidebar = gtk_stack_sidebar_new ();
	gtk_widget_set_margin_bottom(sidebar, SPACING_LARGE);
    gtk_box_pack_start (GTK_BOX (mainbox), sidebar, FALSE, FALSE, 0);


	stack = gtk_stack_new ();
	//gtk_stack_set_transition_type (GTK_STACK (stack), GTK_STACK_TRANSITION_TYPE_SLIDE_UP_DOWN);
	//gtk_stack_set_transition_type (GTK_STACK (stack), GTK_STACK_TRANSITION_TYPE_CROSSFADE);
	gtk_stack_sidebar_set_stack (GTK_STACK_SIDEBAR (sidebar), GTK_STACK (stack));
	gtk_container_set_border_width(GTK_CONTAINER(stack), SPACING_MEDIUM);


	data->stack = stack;
    gtk_box_pack_start (GTK_BOX (mainbox), stack, TRUE, TRUE, 0);


	//common (date + status + amount)
/*	label = gtk_label_new(_("General"));
	page = ui_flt_manage_page_general(&data);
	gtk_notebook_append_page (GTK_NOTEBOOK (notebook), page, label);
*/

	page = ui_flt_manage_part_date(data);
	//gtk_widget_show(GTK_WIDGET(page));
	gtk_stack_add_titled (GTK_STACK (stack), page, "dat", _("Dates"));

	page = ui_flt_manage_part_status(data);
	//gtk_widget_show(GTK_WIDGET(page));
	gtk_stack_add_titled (GTK_STACK (stack), page, "sta", _("Status"));

	page = ui_flt_manage_part_paymode(data);
	//gtk_widget_show(GTK_WIDGET(page));
	gtk_stack_add_titled (GTK_STACK (stack), page, "pmt", _("Payments"));

	page = ui_flt_manage_part_amount(data);
	//gtk_widget_show(GTK_WIDGET(page));
	gtk_stack_add_titled (GTK_STACK (stack), page, "amt", _("Amounts"));

	page = ui_flt_manage_part_text(data);
	//gtk_widget_show(GTK_WIDGET(page));
	gtk_stack_add_titled (GTK_STACK (stack), page, "txt", _("Texts"));

	page = ui_flt_manage_page_category(data);
	//gtk_widget_show(GTK_WIDGET(page));
	gtk_stack_add_titled (GTK_STACK (stack), page, "cat", _("Categories"));

	page = ui_flt_manage_page_payee(data);
	//gtk_widget_show(GTK_WIDGET(page));
	gtk_stack_add_titled (GTK_STACK (stack), page, "pay", _("Payees"));

	data->show_account = show_account;
	if(show_account == TRUE)
	{
		page = ui_flt_manage_page_account(data);
		//gtk_widget_show(GTK_WIDGET(page));
		gtk_stack_add_titled (GTK_STACK (stack), page, "acc", _("Accounts"));
	}

	
	//setup, init and show dialog
	ui_flt_manage_setup(data);
	ui_flt_manage_set(data);

	ui_flt_manage_option_update(dialog, NULL);

	/* signal connect */
    g_signal_connect (data->CY_option[FLT_GRP_STATUS]  , "changed", G_CALLBACK (ui_flt_manage_option_update), NULL);
	g_signal_connect (data->CY_option[FLT_GRP_TYPE]    , "changed", G_CALLBACK (ui_flt_manage_option_update), NULL);
    g_signal_connect (data->CY_option[FLT_GRP_DATE]    , "changed", G_CALLBACK (ui_flt_manage_option_update), NULL);
    g_signal_connect (data->CY_option[FLT_GRP_AMOUNT]  , "changed", G_CALLBACK (ui_flt_manage_option_update), NULL);
    g_signal_connect (data->CY_option[FLT_GRP_PAYMODE] , "changed", G_CALLBACK (ui_flt_manage_option_update), NULL);

    g_signal_connect (data->CY_option[FLT_GRP_PAYEE]   , "changed", G_CALLBACK (ui_flt_manage_option_update), NULL);
    g_signal_connect (data->CY_option[FLT_GRP_CATEGORY], "changed", G_CALLBACK (ui_flt_manage_option_update), NULL);
    g_signal_connect (data->CY_option[FLT_GRP_TEXT]    , "changed", G_CALLBACK (ui_flt_manage_option_update), NULL);

	if(show_account == TRUE)
	{
	    g_signal_connect (data->CY_option[FLT_GRP_ACCOUNT] , "changed", G_CALLBACK (ui_flt_manage_option_update), NULL);

	}



	gtk_widget_show_all (dialog);

	if(!txnmode)
	{
		hb_widget_visible (data->LB_force, FALSE);
		hb_widget_visible (data->GR_force, FALSE);
	}


	if( *data->filter->last_tab != '\0' )
		gtk_stack_set_visible_child_name (GTK_STACK(data->stack), data->filter->last_tab);
	DB( g_print(" set page '%s'\n", data->filter->last_tab) );


	//wait for the user
	gint retval;	// = 55;

	//while( result == 55 )
	//{
		retval = gtk_dialog_run (GTK_DIALOG (dialog));

		switch (retval)
	    {
		case GTK_RESPONSE_ACCEPT:
		   //do_application_specific_something ();
			ui_flt_manage_get(data);
			break;
		//case 55:	reset will be treated in calling dialog
	    }
	//}

	// cleanup and destroy
	//ui_flt_manage_cleanup(&data, result);


	DB( g_print(" free\n") );
	//g_free(data);

	DB( g_print(" destroy\n") );
	gtk_widget_destroy (dialog);

	g_free(data);
	
	DB( g_print(" all ok\n") );

	return retval;
}
